package context;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import context.comparison.ContextComparatorStrategy;

public class Context implements Cloneable {

	public static final int MAX_CONTEXT_SIZE = 4;

	/**
	 * The situations of this context. Each situation depicts a unique type of
	 * environmental information
	 */
	private Map<ContextInfo, List<ContextEntry>> situations;

	/**
	 * A data index that depicts how much data has been perceived before this
	 * context;
	 */
	private int finalDataIdx;

	/**
	 * The strategy used to compare contexts
	 */
	private static Optional<ContextComparatorStrategy> comparisonStrategy = Optional.empty();

	/**
	 * Create a new instance of {@code Context}
	 * 
	 * @param info
	 *            the types of information supported by this context
	 */
	public Context(ContextInfo... info) {
		this.situations = new HashMap<>();

		for (ContextInfo in : info) {
			situations.put(in, new ArrayList<>());
		}
		finalDataIdx = 0;
	}

	public static void setContextComparisonStrategy(ContextComparatorStrategy s) {
		comparisonStrategy = Optional.of(s);
	}

	public boolean isEmpty() {
		boolean empty = true;
		for (ContextInfo inf : situations.keySet()) {
			if (!situations.get(inf).isEmpty()) {
				empty = false;
			}
		}
		return empty;
	}

	/**
	 * Return {@code true} if the context has a number of samples equal or greater
	 * than {@code MAX_CONTEXT_SIZE}.
	 * 
	 * @return
	 */
	public boolean isFull() {
		return size() >= MAX_CONTEXT_SIZE;
	}

	public int size() {
		if (situations.values().isEmpty()) {
			return 0;
		}

		return situations.values().stream().findFirst().get().size();
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		Context copy = new Context(situations.keySet().stream().toArray(ContextInfo[]::new));

		for (Entry<ContextInfo, List<ContextEntry>> entry : situations.entrySet()) {
			copy.situations.put(entry.getKey(), new ArrayList<>(entry.getValue()));
		}
		return copy;
	}

	public void setFinalDataIdx(int finalDataIdx) {
		this.finalDataIdx = finalDataIdx;
	}

	public int getFinalDataIdx() {
		return finalDataIdx;
	}

	/**
	 * Add a data sequence to this context. For each registered data type supported
	 * by this context, a value must be provided.
	 * 
	 * @param seq
	 * @throws MissingInformationException
	 */
	public void addDataSequence(List<ContextEntry> seq) throws MissingInformationException {

		// add the information to this context
		for (ContextEntry entry : seq) {
			// Add the entry, even if empty
			situations.get(entry.getInfo()).add(entry);
		}

		// check if some information has not been provided, thus it has to be estimated
		List<ContextEntry> missingEntries = seq.stream().filter(x -> x.isEmpty()).collect(Collectors.toList());

		if (!missingEntries.isEmpty()) {
			String missingInfoStr = missingEntries.stream().map(x -> x.getInfo().toString())
					.collect(Collectors.joining("; "));

			throw new MissingInformationException("Missing information(s): " + missingInfoStr, seq,
					missingEntries.stream().map(x -> x.getInfo()).toArray(ContextInfo[]::new));
		}
	}

	public void forceAddDataSequence(List<ContextEntry> seq) {

		// add the information to this context
		for (ContextEntry entry : seq) {
			// Add the entry, even if empty
			situations.get(entry.getInfo()).add(entry);
		}

		// ensure the context has a fixed size
		for (ContextInfo info : situations.keySet()) {
			while (situations.get(info).size() > MAX_CONTEXT_SIZE) {
				situations.get(info).remove(0);
			}
		}
	}

	public void forceAddFirst(List<ContextEntry> seq) {

		// add the information to this context
		for (ContextEntry entry : seq) {
			// Add the entry, even if empty
			situations.get(entry.getInfo()).add(0, entry);
		}

		// ensure the context has a fixed size
		for (ContextInfo info : situations.keySet()) {
			while (situations.get(info).size() > MAX_CONTEXT_SIZE) {
				situations.get(info).remove(0);
			}
		}
	}

	public ContextEntry[] getSituation(ContextInfo info) {
		return situations.get(info).stream().toArray(ContextEntry[]::new);
	}

	public double[] getSituationAsDouble(ContextInfo info) {
		synchronized (situations) {
			return situations.get(info).stream().mapToDouble(x -> x.getValue()).toArray();
		}

	}

	public Set<ContextInfo> getSupportedInfo() {
		return situations.keySet();
	}

	public ContextInfo[] getSupportedInfoArray() {
		return situations.keySet().stream().toArray(ContextInfo[]::new);
	}

	/**
	 * Compare this context to the given input context. It returns the list of
	 * information that they do not have in common.
	 * 
	 * @param c
	 * @return
	 */
	public List<ContextInfo> haveSameInfo(Context c) {
		return situations.keySet().stream().filter(x -> !c.getSupportedInfo().contains(x)).collect(Collectors.toList());
	}

	/**
	 * Compare this context to the input context. The data sequences of the same
	 * type are compared using the Dynamic Time Warping (DTW).
	 * 
	 * @param c
	 *            the context to compare to
	 * @return the mean of the DTW between the sequences of the contexts
	 */
	public double compare(Context c) {

		ContextComparatorStrategy s = comparisonStrategy
				.orElseThrow(() -> new IllegalAccessError("No context comparator provided"));

		return s.compare(this, c);
	}

	@Override
	public boolean equals(Object obj) {
		// If the object is compared with itself then return true
		if (obj == this) {
			return true;
		}

		// Check if o is an instance of Complex or not "null instanceof [type]" also
		// returns false
		if (!(obj instanceof Context)) {
			return false;
		}

		return compare((Context) obj) == 0;
	}

	/**
	 * 
	 * @param idx
	 * @return
	 */
	public List<ContextEntry> getSampleAt(int idx) {
		if (idx >= size()) {
			throw new IllegalArgumentException("Specified idx over the context size");
		}

		List<ContextEntry> entries = new ArrayList<>();
		for (Entry<ContextInfo, List<ContextEntry>> entry : situations.entrySet()) {
			entries.add(situations.get(entry.getKey()).get(idx));
		}

		return entries;
	}

	public List<ContextEntry> removeLastSample() {
		List<ContextEntry> entries = new ArrayList<>();

		for (Entry<ContextInfo, List<ContextEntry>> entry : situations.entrySet()) {
			int lastElemIdx = situations.get(entry.getKey()).size() - 1;
			ContextEntry elem = situations.get(entry.getKey()).get(lastElemIdx);
			entries.add(elem);
			situations.get(entry.getKey()).remove(elem);
		}

		return entries;
	}

	public List<ContextEntry> removeFirstSample() {
		List<ContextEntry> entries = new ArrayList<>();

		for (Entry<ContextInfo, List<ContextEntry>> entry : situations.entrySet()) {
			ContextEntry elem = situations.get(entry.getKey()).get(0);
			entries.add(elem);
			situations.get(entry.getKey()).remove(elem);
		}

		return entries;
	}

	public List<ContextEntry> getLastSample() {
		List<ContextEntry> entries = new ArrayList<>();

		for (Entry<ContextInfo, List<ContextEntry>> entry : situations.entrySet()) {
			int lastElemIdx = situations.get(entry.getKey()).size() - 1;
			ContextEntry elem = situations.get(entry.getKey()).get(lastElemIdx);
			entries.add(elem);
		}

		return Collections.unmodifiableList(entries);
	}

	/**
	 * remove all the entry from this context
	 */
	public void clear() {
		for (Entry<ContextInfo, List<ContextEntry>> entry : situations.entrySet()) {
			situations.get(entry.getKey()).clear();
		}

	}

	@Override
	public String toString() {
		List<String> st = new ArrayList<>();

		for (ContextInfo ci : situations.keySet()) {
			st.add(ci.toString() + " -> " + situations.get(ci).stream().map(x -> Double.toString(x.getValue()))
					.collect(Collectors.joining("; ")));
		}
		return st.stream().map(x -> x).collect(Collectors.joining("\n"));
	}
}
