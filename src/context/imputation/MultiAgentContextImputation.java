package context.imputation;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;

import agent.base.Agent;
import context.Context;
import context.ContextEntry;
import context.ContextInfo;

public class MultiAgentContextImputation implements ContextImputationStrategy {

	// [agentContext] -> contesti che somigliano di piu a [lastObservedContext]
	@Override
	public ContextEntry impute(Agent self, Map<Agent, List<Context>> agentContext, Context lastObservedContext,
			ContextInfo missingInfo) {

		List<Context> myBestContexts = agentContext.get(self);
		Map<Integer, List<Pair<Context, Double>>> theBestContextMap = new HashMap<>();

		for (Context c : myBestContexts) {
			theBestContextMap.put(c.getFinalDataIdx(), new ArrayList<>());
			theBestContextMap.get(c.getFinalDataIdx()).add(Pair.of(c, lastObservedContext.compare(c)));

			// other agents
			for (Entry<Agent, List<Context>> e : agentContext.entrySet().stream()
					.filter(x -> x.getKey().getAgentId() != self.getAgentId()).collect(Collectors.toList())) {

				// chaque agent donne sa *photo* la plus proche du contexte qu'on a lui donn�
				Context theBestContext = e.getValue().stream()
						.sorted((c1, c2) -> Double.compare(c.compare(c1), c.compare(c2))).findFirst().get();

				theBestContextMap.get(c.getFinalDataIdx()).add(Pair.of(theBestContext, theBestContext.compare(c)));
			}
		}

		double theNum = 0, theDiv = 0, maxWeight = Double.MIN_VALUE;
		List<Pair<Double, Double>> kv = new ArrayList<>();

		for (Entry<Integer, List<Pair<Context, Double>>> e : theBestContextMap.entrySet()) {
			double weight = e.getValue().stream().mapToDouble(x -> x.getValue()).average().getAsDouble();
			if (weight > maxWeight) {
				maxWeight = weight;
			}
		}

		for (Entry<Integer, List<Pair<Context, Double>>> e : theBestContextMap.entrySet()) {
			Context theContext = myBestContexts.stream().filter(x -> x.getFinalDataIdx() == e.getKey()).findFirst()
					.get();

			double val = theContext
					.getSituationAsDouble(missingInfo)[theContext.getSituationAsDouble(missingInfo).length - 1];

			double weight = e.getValue().stream().mapToDouble(x -> x.getValue()).average().getAsDouble() / maxWeight;
			kv.add(Pair.of(val, weight));
		}

		theNum = kv.stream().mapToDouble(x -> x.getLeft() * x.getRight()).sum();
		theDiv = kv.stream().mapToDouble(x -> x.getRight()).sum();

		double estimation = theNum / theDiv;

		ContextEntry ee = new ContextEntry(missingInfo, Instant.now(), estimation);
		ee.setIsEstimation(true);
		return ee;
	}

	@Override
	public String getName() {
		return "mas";
	}
}
