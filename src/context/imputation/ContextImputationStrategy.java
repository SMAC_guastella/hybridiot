package context.imputation;

import java.util.List;
import java.util.Map;

import agent.base.Agent;
import context.Context;
import context.ContextEntry;
import context.ContextInfo;

public interface ContextImputationStrategy {
	String getName();

	/**
	 * Impute an information to {@code lastObservedContext} using the agents
	 * contexts {@code agentContext}.
	 * 
	 * @param self
	 *            the agent that required the imputation
	 * @param agentContext
	 *            a map where a key is an agent and its value is the list of
	 *            contexts observed by the agent
	 * @param lastObservedContext
	 *            the last context observed by the agent that has to impute a
	 *            missing information
	 * 
	 * @param missingInfo
	 *            the type of information to be imputed
	 * 
	 * @return
	 */
	ContextEntry impute(Agent self, Map<Agent, List<Context>> agentContext, Context lastObservedContext,
			ContextInfo missingInfo);
}
