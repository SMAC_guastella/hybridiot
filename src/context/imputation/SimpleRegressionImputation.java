package context.imputation;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import org.apache.commons.math3.stat.regression.SimpleRegression;

import agent.base.Agent;
import agent.crossValidation.CrossValidationAgent;
import context.Context;
import context.ContextEntry;
import context.ContextInfo;

public class SimpleRegressionImputation implements ContextImputationStrategy {

	private final static SimpleRegression regr = new SimpleRegression();

	@Override
	public ContextEntry impute(Agent self, Map<Agent, List<Context>> agentContext, Context lastObservedContext,
			ContextInfo missingInfo) {

		regr.clear();

		for (Agent a : agentContext.keySet()) {
			List<Context> cc = agentContext.get(a);

			for (int i = 0; i < cc.size(); i++) {
				double[] range = IntStream.rangeClosed(cc.get(i).getFinalDataIdx() - Context.MAX_CONTEXT_SIZE + 1,
						cc.get(i).getFinalDataIdx()).mapToDouble(x -> x).toArray();

				for (int j = 0; j < range.length; j++) {
					regr.addData(range[j], cc.get(i).getSituationAsDouble(missingInfo)[j]);
				}
			}

		}

		int x = ((CrossValidationAgent) self).getRealSensor().getCurrentSampleIdx();
		double intercept = regr.getIntercept();
		double slope = regr.getSlope();
		double estimation = intercept + slope * x;

		ContextEntry ee = new ContextEntry(missingInfo, Instant.now(), estimation);
		ee.setIsEstimation(true);
		return ee;

	}

	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}
}
