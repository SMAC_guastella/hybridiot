package context.imputation;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;

import agent.base.Agent;
import context.Context;
import context.ContextEntry;
import context.ContextInfo;

public class ContextVarImputation implements ContextImputationStrategy {

	@Override
	public ContextEntry impute(Agent self, Map<Agent, List<Context>> agentContext, Context lastObservedContext,
			ContextInfo missingInfo) {

		// Get the most similar contexts of the agent that needs to estimate the
		// information
		List<Context> myBestContexts = agentContext.get(self);

		Map<Integer, List<Pair<Context, Double>>> theBestContextMap = new HashMap<>();

		// Get the last perceived value of the agent
		double lastPerceivedValue = lastObservedContext
				.getSituationAsDouble(missingInfo)[lastObservedContext.getSituationAsDouble(missingInfo).length - 1];

		// For each similar context
		for (Context c : myBestContexts) {
			// put in a map the index of the context c and the distance between the last
			// observed context
			theBestContextMap.put(c.getFinalDataIdx(), new ArrayList<>());
			theBestContextMap.get(c.getFinalDataIdx()).add(Pair.of(c, lastObservedContext.compare(c)));

			// do the same for the other agents
			for (Entry<Agent, List<Context>> e : agentContext.entrySet().stream()
					.filter(x -> x.getKey().getAgentId() != self.getAgentId()).collect(Collectors.toList())) {

				// chaque agent donne sa photo la plus proche du contexte qu'on a lui donn�
				Context theBestContext = e.getValue().stream()
						.sorted((c1, c2) -> Double.compare(c.compare(c1), c.compare(c2))).findFirst().get();

				theBestContextMap.get(c.getFinalDataIdx()).add(Pair.of(theBestContext, theBestContext.compare(c)));
			}
		}

		double theNum = 0, theDiv = 0, maxWeight = Double.MIN_VALUE;
		List<Pair<Double, Double>> kv = new ArrayList<>();

		// Normalize the distances between contexts in order to have values in [0,1]
		for (Entry<Integer, List<Pair<Context, Double>>> e : theBestContextMap.entrySet()) {
			double weight = e.getValue().stream().mapToDouble(x -> x.getValue()).average().getAsDouble();
			if (weight > maxWeight) {
				maxWeight = weight;
			}
		}

		// For each context of the agent (which ID is the value of the map), weight the
		// average distance among the most similar contexts (of the agent itself and the
		// neighbor agents) and keep the weight in a list
		for (Entry<Integer, List<Pair<Context, Double>>> e : theBestContextMap.entrySet()) {
			Context theContext = myBestContexts.stream().filter(x -> x.getFinalDataIdx() == e.getKey()).findFirst()
					.get();

			// Take the >value< difference between the k and k-1 samples for each context
			double val = theContext
					.getSituationAsDouble(missingInfo)[theContext.getSituationAsDouble(missingInfo).length - 1];

			double val1 = theContext
					.getSituationAsDouble(missingInfo)[theContext.getSituationAsDouble(missingInfo).length - 2];

			double valDiff = val1 - val;

			// take the average weight of the most similar context
			double weight = 1
					- (e.getValue().stream().mapToDouble(x -> x.getValue()).average().getAsDouble() / maxWeight);

			// Add the value difference and the weight in a list
			kv.add(Pair.of(valDiff, weight));
		}

		// evaluate the weighted average of the difference values and the weights for
		// the used contexts
		theNum = kv.stream().mapToDouble(x -> x.getLeft() * x.getRight()).sum();
		theDiv = kv.stream().mapToDouble(x -> x.getRight()).sum();

		// Impute the missing value
		double estimation = lastPerceivedValue + theNum / theDiv;

		// Create and return a new context entry
		ContextEntry ee = new ContextEntry(missingInfo, Instant.now(), estimation);
		ee.setIsEstimation(true);
		return ee;
	}

	@Override
	public String getName() {
		return null;
	}
}
