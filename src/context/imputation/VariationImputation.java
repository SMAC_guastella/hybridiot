package context.imputation;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

import org.apache.commons.math3.stat.StatUtils;
import org.apache.commons.math3.stat.descriptive.rank.Median;

import agent.base.Agent;
import context.Context;
import context.ContextEntry;
import context.ContextInfo;
import smile.math.Random;

public class VariationImputation implements ContextImputationStrategy {

	private final static Median median = new Median();

	@Override
	public ContextEntry impute(Agent self, Map<Agent, List<Context>> agentContext, Context lastObservedContext,
			ContextInfo missingInfo) {

		double lastPerceivedValue = lastObservedContext
				.getSituationAsDouble(missingInfo)[lastObservedContext.getSituationAsDouble(missingInfo).length - 1];

		Map<Agent, Double> directionSuggestions = new HashMap<>();

		// 10 best context per agent (neighbors)
		for (Entry<Agent, List<Context>> e : agentContext.entrySet()) {
			List<Context> bestContexts = e.getValue().stream()
					.sorted((a, b) -> Double.compare(a.compare(lastObservedContext), b.compare(lastObservedContext)))
					.collect(Collectors.toList());

			// measure the variations of the last 2 samples for EACH context in bestContexts
			List<Double> variations = new ArrayList<>();
			for (Context c : bestContexts) {
				double lastVal = c.getSituationAsDouble(missingInfo)[c.size() - 1];
				double theVariation = lastVal - c.getSituationAsDouble(missingInfo)[c.size() - 2];

				if(Arrays.asList(c.getSituation(missingInfo)).stream().filter(x->x.isEstimation()).findAny().isPresent())
				{
					continue;
				}
				
				variations.add(theVariation);
			}

			// save the mean direction suggested by each agent
			directionSuggestions.put(e.getKey(), variations.stream().mapToDouble(x -> x).average().orElse(0));
		}

		List<Context> theBestContexts = agentContext.entrySet().stream().map(x -> x.getValue()).flatMap(List::stream)
				.sorted((c1, c2) -> Double.compare(lastObservedContext.compare(c1), lastObservedContext.compare(c2)))
				.collect(Collectors.toList());

		Context theBestContext = Optional.ofNullable(theBestContexts.get(0)).get();

		double direction = directionSuggestions.values().stream().mapToDouble(x -> x).average().orElse(0);

		// System.err.println("DIRECTION -------------> "+ Double.toString(direction));

		/*
		 * double estimation = theBestContext
		 * .getSituationAsDouble(missingInfo)[theBestContext.getSituationAsDouble(
		 * missingInfo).length - 1];
		 */
		/*
		 * double estimation = bestContext.get()
		 * .getSituationAsDouble(missingInfo)[bestContext.get().getSituationAsDouble(
		 * missingInfo).length - 1];
		 */
		double[] bestSituation = theBestContext.getSituationAsDouble(missingInfo);
		
		//double estimation = bestSituation[bestSituation.length-1] + direction;

		//double estimation = bestSituation[new Random(System.currentTimeMillis()).nextInt(bestSituation.length - 1)];
		double estimation = lastPerceivedValue + direction;

		ContextEntry ee = new ContextEntry(missingInfo, Instant.now(), estimation);
		ee.setIsEstimation(true);
		
		return ee;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}
}
