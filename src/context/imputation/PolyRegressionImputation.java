package context.imputation;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import org.apache.commons.math3.fitting.PolynomialCurveFitter;
import org.apache.commons.math3.fitting.WeightedObservedPoints;
import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression;
import org.apache.commons.math3.stat.regression.SimpleRegression;

import agent.base.Agent;
import agent.crossValidation.CrossValidationAgent;
import context.Context;
import context.ContextEntry;
import context.ContextInfo;

public class PolyRegressionImputation implements ContextImputationStrategy {

	private final static SimpleRegression regr = new SimpleRegression();
	private final static int POLY_DEGREE = 2;
		
	
	private final static WeightedObservedPoints obs = new WeightedObservedPoints();

	@Override
	public ContextEntry impute(Agent self, Map<Agent, List<Context>> agentContext, Context lastObservedContext,
			ContextInfo missingInfo) {

		obs.clear();

		regr.clear();
		OLSMultipleLinearRegression regression = new OLSMultipleLinearRegression();
		
		
		for (Agent a : agentContext.keySet()) {
			List<Context> cc = agentContext.get(a);

			for (int i = 0; i < cc.size(); i++) {
				double[] range = IntStream.rangeClosed(cc.get(i).getFinalDataIdx() - Context.MAX_CONTEXT_SIZE + 1,
						cc.get(i).getFinalDataIdx()).mapToDouble(x -> x).toArray();

				for (int j = 0; j < range.length; j++) {
					obs.add(range[j], cc.get(i).getSituationAsDouble(missingInfo)[j]);
//					regression.newSampleData(y, x);
				}
			}

		}
	
		
		
		final PolynomialCurveFitter fitter = PolynomialCurveFitter.create(POLY_DEGREE);
        final double[] fitted = fitter.fit(obs.toList());
        
        
        
        
       /* double estimation = 0.0;
        int x = ((CrossValidationAgent) self).getRealSensor().getCurrentSampleIdx() - POLY_DEGREE -1;
        for (int i = 0; i < fitted.length; i++, x++) {
        	estimation += fitted[i] * x;
		}
        */
		
		double intercept = regr.getIntercept();
		double slope = regr.getSlope();
		//double estimation = intercept + slope * x;

		double estimation = fitted[fitted.length-1];
		
		ContextEntry ee = new ContextEntry(missingInfo, Instant.now(), estimation);
		ee.setIsEstimation(true);
		return ee;

	}

	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}
}
