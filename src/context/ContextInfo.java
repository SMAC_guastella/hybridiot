package context;

import java.util.Objects;

public enum ContextInfo {
	/* WOLLONGONG */
	// HUMIDITY("humidity"), TEMP("temp"), NULL("");

	/*******/

	/* EMILIA ROMAGNA */
	HUMIDITY("humidity"), TEMP("temperature"), RADIATION("radiation"), WINDSPEED("windSpeed"), NULL("");

	private final String name;

	private ContextInfo(String name) {
		this.name = Objects.requireNonNull(name);
	}

	@Override
	public String toString() {
		return name.toUpperCase();
	}

	public static ContextInfo fromString(String s) {
		for (ContextInfo instance : ContextInfo.values()) {
			if (instance.toString().equals(s.trim().toUpperCase())) {
				return instance;
			}
		}
		throw new IllegalArgumentException("no info corresponding to specified string: \"" + s + "\"");
	}

}
