package context.comparison;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import context.Context;
import context.ContextInfo;

public class ContextComparatorImpl implements ContextComparatorStrategy {

	@Override
	public double compare(Context c1, Context c2) {

		if (c1.isEmpty() || c2.isEmpty()) {
			return 0;
		}

		// get the information that both contexts have in common
		List<ContextInfo> commonsInfo = c1.getSupportedInfo().stream().filter(x -> c2.getSupportedInfo().contains(x))
				.collect(Collectors.toList());

		// loop over the common informations between the contexts
		List<Double> areas = new ArrayList<>();

		// evaluate each information
		for (ContextInfo info : commonsInfo) {
			double[] c1InfoSeq = c1.getSituationAsDouble(info);
			double[] meanPoly = c2.getSituationAsDouble(info);
			List<Double> polyPointsX = new ArrayList<>();
			for (int i = 0; i < meanPoly.length; i++) {
				polyPointsX.add(Math.abs(meanPoly[i] - c1InfoSeq[i]));
			}

			areas.add(polyPointsX.stream().mapToDouble(x -> x).average().getAsDouble() / polyPointsX.size());
		}

		double avg = areas.stream().mapToDouble(x -> x).average().orElse(0);

		return avg;
	}

}
