package context.comparison;

import java.util.HashSet;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.stream.Collectors;

import context.Context;
import context.ContextInfo;
import smile.math.distance.DynamicTimeWarping;

public class DynamicTimeWarpContextComparator implements ContextComparatorStrategy {
	@Override
	public double compare(Context c1, Context c2) {

		if (c1.isEmpty() || c2.isEmpty()) {
			return Double.MAX_VALUE;
		}
		
		Set<Double> scores = new HashSet<>();
		Set<ContextInfo> commonsInfo = c1.getSupportedInfo().stream().filter(x -> c2.getSupportedInfo().contains(x))
				.collect(Collectors.toSet());

		for (ContextInfo c : commonsInfo) {
			double[] d1 = c1.getSituationAsDouble(c);
			double[] d2 = c2.getSituationAsDouble(c);
			scores.add(DynamicTimeWarping.d(d1, d2));
		}

		OptionalDouble vv = scores.stream().mapToDouble(x -> x).average();

		if (vv.isPresent()) {
			return vv.getAsDouble() / commonsInfo.size();
		}

		return Double.MAX_VALUE;
	}
}
