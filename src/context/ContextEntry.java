package context;

import java.time.Instant;

public class ContextEntry {
	/**
	 * The type of information of this entry
	 */
	private final ContextInfo info;

	/**
	 * the value of this entry
	 */
	private final Double value;

	/**
	 * The time instant when this information has been perceived
	 */
	private final Instant perceptionInstant;

	private boolean isEmpty;

	private boolean isEstimation;

	/**
	 * Create a new context entry 
	 * 
	 * @param info
	 * @param perceptionInstant
	 * @param value
	 */
	public ContextEntry(ContextInfo info, Instant perceptionInstant, Double value) {
		this.info = info;
		this.value = value;
		this.perceptionInstant = perceptionInstant;

		isEmpty = Double.isNaN(value);
		isEstimation = false;
	}

	/**
	 * Create a new empty context entry
	 * @param info
	 * @return
	 */
	public static ContextEntry empty(ContextInfo info) {
		ContextEntry ce = new ContextEntry(info, Instant.now(), Double.NaN);
		ce.isEmpty = true;
		ce.isEstimation = false;
		return ce;
	}

	/**
	 * Return {@code true} if this entry contains a NaN value or no value has been
	 * set yet, {@code false} otherwise.
	 * 
	 * @return
	 */
	public boolean isEmpty() {
		return isEmpty;
	}

	public ContextInfo getInfo() {
		return info;
	}

	public Double getValue() {
		return value;
	}

	public Instant getPerceptionInstant() {
		return perceptionInstant;
	}

	public boolean isEstimation() {
		return isEstimation;
	}

	public void setIsEstimation(boolean isEstimation) {
		this.isEstimation = isEstimation;
	}

	@Override
	public String toString() {
		return "(" + info + "=" + value + ")";
	}

}
