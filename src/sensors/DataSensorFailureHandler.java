package sensors;

import agent.base.Agent;

public interface DataSensorFailureHandler {

	void handleSensorFailure(Agent a, Exception e, DataSensor s);
}
