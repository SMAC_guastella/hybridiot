package sensors;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import context.ContextEntry;
import context.ContextInfo;
import device.Device;

public class FileDataSensor implements DataSensor, Device {

	/**
	 * The complete path to the dataset
	 */
	private String fname;

	/**
	 * The file object that points to the dataset
	 */
	private File file;

	/**
	 * A boolean flag that indicates wheter this sensor is active or not
	 */
	private boolean active;

	private Map<ContextInfo, Boolean> activeSensors;
	private Map<ContextInfo, Integer> dataIdx;

	/**
	 * Set of sensors ID in the provided file
	 */
	private Set<String> availableSensorsID;

	/**
	 * The number of samples of this dataset
	 */
	private int dataSize;

	/**
	 * ID of the used sensors (name of city, station, sensor...)
	 */
	private String ID;

	/**
	 * Data map with type of information as key and list of double values as value
	 */
	private Map<ContextInfo, List<Double>> dataTable;

	/**
	 * An optional binary vector that specify when this sensor works or not. If the
	 * value at the i-th position is {@code true}, this sensors simulate a
	 * malfunction, thus providing a null data. Otherwise, if {@code false}, the
	 * data is provided by the real sensor. <br/>
	 * <br/>
	 * 
	 * <b>Used when doing cross-validation.<b/>
	 */
	private Optional<Boolean[]> testSetIDX;

	/**
	 * Constructor for {@code FileDataReceiver} class
	 * 
	 * @param filename
	 */
	private FileDataSensor(String filename, String columnsSeparator) {
		availableSensorsID = new HashSet<>();
		dataTable = new HashMap<>();
		activeSensors = new HashMap<>();
		dataIdx = new HashMap<>();
		fname = filename;
		file = new File(fname);
		ID = "";
		dataSize = 0;
		active = true;
		testSetIDX = Optional.empty();
	}

	/**
	 * Static factory method for {@code FileDataReceiver} class
	 * 
	 * @param filename
	 * @param sensorID
	 * @return
	 * @throws Exception
	 */
	public static FileDataSensor getNew(String filename, String sensorID, String columnSeparator) throws Exception {
		FileDataSensor fd = new FileDataSensor(filename, columnSeparator);

		List<String> lines = Files.readAllLines(fd.file.toPath(), StandardCharsets.UTF_8);
		fd.availableSensorsID = lines.stream().map(x -> x.split(columnSeparator)[0]).collect(Collectors.toSet());

		if (!fd.availableSensorsID.contains(sensorID)) {
			throw new IllegalArgumentException("Specified sensor ID has not been found in file.");
		}
		fd.ID = sensorID;

		// parse data line by line
		for (String line : lines) {
			String[] words = line.split(columnSeparator);

			// Find the line associated to the specified sensor
			if (!words[0].equals(sensorID)) {
				continue;
			}

			String[] dataStr = Arrays.copyOfRange(words, 2, words.length);
			ContextInfo info = ContextInfo.fromString(words[1].trim());
			List<Double> dataDouble = Arrays.asList(dataStr).stream().map(x -> Double.parseDouble(x))
					.collect(Collectors.toList());

			fd.dataTable.put(info, dataDouble);
			fd.dataSize = fd.dataTable.get(info).size();
			fd.dataIdx.put(info, 0);
			fd.activeSensors.put(info, true);
		}
		return fd;
	}
	
	@Override
	public int getSamplesCount() {
		return dataSize;
	}

	@Override
	public int getCurrentSampleIdx(ContextInfo info) {
		return dataIdx.get(info);
	}

	@Override
	public int getCurrentSampleIdx() {
		return dataIdx.values().stream().findFirst().get();
	}

	public static Set<ContextInfo> getAvailableInformation(String filename, String columnSeparator) throws IOException {
		List<String> lines = Files.readAllLines(new File(filename).toPath(), StandardCharsets.UTF_8);

		Set<ContextInfo> info = new HashSet<>();

		for (String line : lines) {
			String[] words = line.split(columnSeparator);
			info.add(ContextInfo.fromString(words[1]));
		}

		return info;
	}

	@Override
	public ContextEntry receiveData(ContextInfo info) throws EOFException {

		int idx = getDataIDX(info);

		if (idx >= dataTable.get(info).size()) {
			throw new EOFException("Reached EOF.");
		}

		// If a test set is provided, turn on/off accordingly
		if (testSetIDX.isPresent()) {
			if (testSetIDX.get()[idx]) {
				turnOff(info);
			} else {
				turnOn(info);
			}
		}

		if (activeSensors.get(info)) {
			ContextEntry c = new ContextEntry(info, Instant.now(), dataTable.get(info).get(idx));
			return c;
		}

		return ContextEntry.empty(info);
	}

	public String getFname() {
		return fname;
	}

	public Integer getDataIDX(ContextInfo info) {
		return dataIdx.get(info);
	}

	public void nextSample() {
		for (ContextInfo info : dataIdx.keySet()) {
			dataIdx.put(info, dataIdx.get(info) + 1);
		}

	}

	/**
	 * Reset the data indexes to the first sample
	 */
	public void resetDataIdx() {
		for (ContextInfo inf : dataIdx.keySet()) {
			dataIdx.put(inf, 0);
		}
	}

	/**
	 * Get the set of available IDs (name of city, station, sensor...) within the
	 * chosen file
	 */
	public Set<String> getAvailableSensorsID() {
		return availableSensorsID;
	}

	/**
	 * Get the ID of the used sensors (name of city, station, sensor...)
	 */
	public String getID() {
		return ID;
	}

	public double[] getDataAsDouble(ContextInfo info) {
		return dataTable.get(info).stream().mapToDouble(x -> x).toArray();
	}

	@Override
	public boolean isActive() {
		return active;
	}

	@Override
	public void turnOn() {
		if (testSetIDX.isPresent()) {
			throw new IllegalAccessError("When a test set is provided, user cannot turn on/off sensor.");
		}

		activeSensors.forEach((k, v) -> activeSensors.put(k, true));
	}

	@Override
	public void turnOff() {
		if (testSetIDX.isPresent()) {
			throw new IllegalAccessError("When a test set is provided, user cannot turn on/off sensor.");
		}
		activeSensors.forEach((k, v) -> activeSensors.put(k, false));

	}

	@Override
	public void turnOn(ContextInfo info) {
		/*
		 * if (testSetIDX.isPresent()) { throw new
		 * IllegalAccessError("When a test set is provided, user cannot turn on/off sensor."
		 * ); }
		 */
		activeSensors.put(info, true);
	}

	@Override
	public void turnOff(ContextInfo info) {
		/*
		 * if (testSetIDX.isPresent()) { throw new
		 * IllegalAccessError("When a test set is provided, user cannot turn on/off sensor."
		 * ); }
		 */
		activeSensors.put(info, false);
	}

	public Boolean[] getTestSetIDX() {
		return testSetIDX.orElseThrow(() -> new IllegalAccessError("No test set provided."));
	}

	public void setTestSetIDX(Boolean[] s) {
		testSetIDX = Optional.ofNullable(s);
	}
}
