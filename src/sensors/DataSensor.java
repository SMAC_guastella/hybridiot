package sensors;

import context.ContextEntry;
import context.ContextInfo;

public interface DataSensor {
	ContextEntry receiveData(ContextInfo info) throws Exception;

	int getSamplesCount();

	int getCurrentSampleIdx(ContextInfo info);

	int getCurrentSampleIdx();

}
