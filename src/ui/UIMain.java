package ui;

import java.util.ArrayList;
import java.util.List;

import agent.LearnerAgent;
import agent.base.AgentFactory;
import agent.base.AgentListener;
import agent.crossValidation.CrossValidationAgent;
import context.Context;
import context.ContextEntry;
import context.ContextInfo;
import context.comparison.DynamicTimeWarpContextComparator;
import context.imputation.SimpleRegressionImputation;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class UIMain extends Application implements AgentListener {

	// private static final String CSV_DATASET_FNAME =
	// "C:\\Users\\dguastel\\Desktop\\datasetWollongong.csv";
	// private static final CSV_COL_SEPARATOR = ";";

	private static final String CSV_DATASET_FNAME = "C:\\Users\\dguastel\\ownCloud\\travail-sync\\src\\matlab\\historical prediction\\TempForecastTest\\kfold_noVar_multiVariable\\DATASET\\datasetARPAEmultiVar.csv";
	private static final String CSV_COL_SEPARATOR = ",";

	private LearnerAgent ag1;

	private MainWindowController controller;

	void initializeAgent(String SensorName, ContextInfo[] info) {
		Context.setContextComparisonStrategy(new DynamicTimeWarpContextComparator());
		try {
			ag1 = AgentFactory.getRealAgent(CSV_DATASET_FNAME, SensorName, CSV_COL_SEPARATOR, info);
		} catch (Exception e) {
			e.printStackTrace();
		}

		ag1.setImputationStrategy(new SimpleRegressionImputation());
		ag1.setListener(this);
	}

	@Override
	public void init() throws Exception {
		Parameters params = getParameters();
		List<String> list = new ArrayList<>(params.getRaw());
		String sensor = list.remove(0);
		ContextInfo[] ui = list.stream().map(x -> ContextInfo.fromString(x)).toArray(ContextInfo[]::new);
		initializeAgent(sensor, ui);
	}

	@Override
	public void start(Stage primaryStage) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/MainWindow.fxml"));
			Parent root = loader.load();
			Scene scene = new Scene(root, 840, 535);
			primaryStage.setScene(scene);

			controller = loader.getController();
			controller.setAgent(ag1);
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void perceived(List<ContextEntry> entries, int currentSampleIdx) {
		controller.perceivedData(entries);
		
	}
	
	public void perceivedDataAtIDX(int numDataSamples, int currentSampleIdx) {
		//double progress = (double) currentSampleIdx / (double) numDataSamples;
		//controller.setProgress(progress);
	}

	@Override
	public void addedContext(Context context) {
		controller.addedContext(context);
	}
	
	@Override
	public void terminated(CrossValidationAgent ag) {
		// TODO Auto-generated method stub
		
	}

	public static void main(String[] args) {
		//String[] argss = { "Finale Emilia", ContextInfo.TEMP.toString(), ContextInfo.HUMIDITY.toString(), ContextInfo.RADIATION.toString(), ContextInfo.WINDSPEED.toString() };
		//Application.launch(UIMain.class, argss);
	}

}
