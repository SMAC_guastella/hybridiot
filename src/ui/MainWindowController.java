package ui;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import agent.LearnerAgent;
import agent.base.Agent;
import context.Context;
import context.ContextEntry;
import context.ContextInfo;
import device.Device;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Slider;
import javafx.scene.control.Spinner;
import javafx.scene.paint.Color;
import sensors.FileDataSensor;

public class MainWindowController {
	private static final Color ESTIMATION_DOT_COLOR = Color.BLACK;
	private static final int LINECHART_VISIBLE_ELEMS_COUNT = 40;

	private static final String CSV_DATASET_FNAME = "C:\\Users\\dguastel\\ownCloud\\travail-sync\\src\\matlab\\historical prediction\\TempForecastTest\\kfold_noVar_multiVariable\\DATASET\\datasetARPAEmultiVar.csv";
	private static final String CSV_COL_SEPARATOR = ",";

	LearnerAgent ag1;

	@FXML
	Button startBtn;

	@FXML
	Button stopBtn;

	@FXML
	Button startDeviceBtn;

	@FXML
	Button stopDeviceBtn;

	@FXML
	ListView<Context> contextsListView;
	ObservableList<Context> contextList;

	@FXML
	ListView<ContextInfo> availableInfoListView;
	ObservableList<ContextInfo> availableInfoList;

	@FXML
	LineChart<Number, Number> dataChart;

	@FXML
	Slider reasoningCycleMSSlider;

	@FXML
	Slider timeBeforePerceivingMSSlider;

	@FXML
	Spinner<Number> contextSizeSpinner;

	@FXML
	ProgressIndicator progressIndicator;

	@FXML
	ChoiceBox<String> contextFilterChoiceBox;
	ObservableList<ContextInfo> contextFilterList;

	Map<String, XYChart.Series<Number, Number>> chartsSeries;
	static int idx_chart = 0;

	public MainWindowController() {
		contextList = FXCollections.observableArrayList();
		availableInfoList = FXCollections.observableArrayList();
		contextFilterList = FXCollections.observableArrayList();
		chartsSeries = new HashMap<>();

		System.err.println("CONTROLLER created");
	}

	@FXML
	private void initialize() {
		contextsListView.setCellFactory(param -> new ListCell<Context>() {
			protected void updateItem(Context item, boolean empty) {
				super.updateItem(item, empty);

				if (empty || item == null) {
					setText(null);
				} else {

					String valueStr = Arrays.stream(item.getSituationAsDouble(ContextInfo.TEMP)).boxed()
							.map(x -> Double.toString(x)).collect(Collectors.joining(", "));
					setText(ContextInfo.TEMP + "-> [" + valueStr + "]");
				}

			};
		});

		reasoningCycleMSSlider.setValue(Agent.MS_REASONING_CYCLE_INTERVAL);
		reasoningCycleMSSlider.valueProperty().addListener(new ChangeListener<Number>() {
			public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
				Agent.MS_REASONING_CYCLE_INTERVAL = ((Double) new_val).intValue();
			}
		});

		timeBeforePerceivingMSSlider.setValue(Agent.MS_BEFORE_PERCEIVE);
		timeBeforePerceivingMSSlider.valueProperty().addListener(new ChangeListener<Number>() {
			public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
				Agent.MS_BEFORE_PERCEIVE = ((Double) new_val).intValue();
			}
		});

		dataChart.setAnimated(false);
		dataChart.getYAxis().setAutoRanging(true);
		((NumberAxis) dataChart.getYAxis()).setForceZeroInRange(false);
		((NumberAxis) dataChart.getXAxis()).setForceZeroInRange(false);

		try {
			for (ContextInfo info : FileDataSensor.getAvailableInformation(CSV_DATASET_FNAME, CSV_COL_SEPARATOR)) {
				XYChart.Series<Number, Number> series = new XYChart.Series<>();
				series.setName(info.toString().toLowerCase());

				chartsSeries.put(info.toString(), series);
				dataChart.getData().add(series);

				availableInfoList.add(info);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		availableInfoListView.setItems(availableInfoList);
		contextsListView.setItems(contextList);
	}

	@FXML
	public void onStartBtnClick(ActionEvent ev) {
		System.err.println("start agent clicked");

		Platform.runLater(() -> {
			if (!ag1.isActive()) {
				ag1.start();
			} else {
				ag1.resumeAgent();
			}
		});
	}

	@FXML
	public void onStopBtnClick(ActionEvent ev) {
		System.err.println("stop agent clicked");
		Platform.runLater(() -> ag1.pauseAgent());
	}

	@FXML
	public void onStartDeviceBtnClick(ActionEvent ev) {
		System.err.println("start device clicked");
		Platform.runLater(() -> {
			ContextInfo inf = availableInfoListView.getSelectionModel().getSelectedItem();
			((Device) ag1.getRealSensor()).turnOn(inf);
		});
	}

	@FXML
	public void onStopDeviceBtnClick(ActionEvent ev) {
		System.err.println("stop device clicked");
		Platform.runLater(() -> {
			ContextInfo inf = availableInfoListView.getSelectionModel().getSelectedItem();
			((Device) ag1.getRealSensor()).turnOff(inf);
		});
	}

	public void setProgress(double progress) {
		progressIndicator.setProgress(progress);

	}

	public void perceivedData(List<ContextEntry> entries) {
		Platform.runLater(() -> {
			for (ContextEntry entry : entries) {
				XYChart.Data<Number, Number> dd = new XYChart.Data<>(idx_chart, entry.getValue());
				XYChart.Series<Number, Number> series = chartsSeries.get(entry.getInfo().toString());

				series.getData().add(dd);

				if (entry.isEstimation()) {
					String hex = String.format("#%02x%02x%02x", (int) (ESTIMATION_DOT_COLOR.getRed() * 255),
							(int) (ESTIMATION_DOT_COLOR.getGreen() * 255),
							(int) (ESTIMATION_DOT_COLOR.getBlue() * 255));

					Node lineSymbol = dd.getNode().lookup(".chart-line-symbol");
					lineSymbol.setStyle("-fx-background-color: " + hex + ", white;");
				}

				if (series.getData().size() > LINECHART_VISIBLE_ELEMS_COUNT) {
					series.getData().remove(0);
				}
			}
		});
		idx_chart++;
	}

	public void addedContext(Context context) {
		Platform.runLater(() -> contextList.add(context));
	}

	public void setAgent(LearnerAgent ag) {
		this.ag1 = ag;
	}
}
