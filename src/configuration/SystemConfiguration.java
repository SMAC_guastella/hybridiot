package configuration;

import java.util.Arrays;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import context.imputation.ContextVarImputation;

public class SystemConfiguration {

	public static Class IMPUTATION_STRATEGY = ContextVarImputation.class;
	public static String CSV_DATASET_FNAME = "C:\\Users\\dguastel\\ownCloud\\travail-sync\\src\\dataset\\datasetARPAEmultiVar232samples.csv";
	public static String OUT_ESTIMATION_FOLDER = "C:\\Users\\dguastel\\estimations";
	public static String CSV_COL_SEPARATOR = ";";
	public static int NUM_FOLDS = 5;// avant: 3
	public static String[] SENSORS_ID_TO_USE = new String[] { "Finale Emilia" };
	
	
	/**
	 * 
	 * @return
	 */
	public static Options configParameters() {
		Options options = new Options();

		final Option strategy = Option.builder("imputationStrategy")
				.desc("Imputation strategy used by agents: recursif (r) / memoization (m)").hasArg(true).argName("st")
				.required(true).build();

		final Option inputFname = Option.builder("inputFname").desc("Input CSV file name").hasArg(true).argName("fname")
				.required(false).build();

		final Option colSeparator = Option.builder("columnSep").desc("Column separator of the input CSV file")
				.hasArg(true).argName("sep").required(false).build();

		final Option outputPath = Option.builder("outPath").desc("Output folder where to save the results").hasArg(true)
				.argName("path").required(false).build();

		final Option sensorsID = Option.builder("sensorsIDs")
				.desc("List of sensors ID to be used. One agent will be created for each sensor ID").hasArg(true)
				.argName("IDs").required(false).build();

		// Set option c to take 1 to oo arguments
		sensorsID.setArgs(Option.UNLIMITED_VALUES);

		options.addOption(strategy);
		options.addOption(inputFname);
		options.addOption(colSeparator);
		options.addOption(outputPath);
		options.addOption(sensorsID);

		return options;
	}

	/**
	 * 
	 * @param args
	 */
	public static void parseParameters(String[] args) {
		final Options options = configParameters();

		final CommandLineParser parser = new DefaultParser();
		CommandLine line = null;
		try {
			line = parser.parse(options, args);
		} catch (ParseException parseException) {
			System.err.println("ERROR: Unable to parse command-line arguments " + Arrays.toString(args) + " due to: "
					+ parseException);

			// automatically generate the help statement
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("ContextTest", options);

			// System.exit(-1);
		}

		// Configure the parameters

		if (line.hasOption("imputationStrategy")) {
			try {
				IMPUTATION_STRATEGY = Class.forName(line.getOptionValue("imputationStrategy"));
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}

		if (line.hasOption("inputFname")) {
			CSV_DATASET_FNAME = line.getOptionValue("inputFname");
		}
		if (line.hasOption("columnSep")) {
			CSV_COL_SEPARATOR = line.getOptionValue("columnSep");
		}
		if (line.hasOption("outPath")) {
			OUT_ESTIMATION_FOLDER = line.getOptionValue("outPath");
		}

		if (line.hasOption("sensorsIDs")) {
			SENSORS_ID_TO_USE = line.getOptionValues("sensorsIDs");
		}
	}
}
