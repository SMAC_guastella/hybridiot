package entryPoint;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import agent.LearnerAgent;
import agent.base.Agent;
import agent.base.AgentFactory;
import agent.base.AgentListener;
import agent.crossValidation.CrossValidationAgent;
import context.Context;
import context.ContextEntry;
import context.ContextInfo;
import context.comparison.ContextComparatorImpl;
import context.comparison.DynamicTimeWarpContextComparator;
import context.imputation.ContextVarImputation;
import context.imputation.SimpleRegressionImputation;
import context.imputation.VariationImputation;

public class MainWollongong {

	//private static final String CSV_DATASET_FNAME = "C:\\Users\\dguastel\\ownCloud\\travail-sync\\src\\matlab\\historical prediction\\TempForecastTest\\kfold_noVar_multiVariable\\DATASET\\datasetWollongong_HumAndTemp.csv";
	
	//step 23 -> 23*30=690sec == 11,5 min
	private static final String CSV_DATASET_FNAME = "C:\\Users\\dguastel\\ownCloud\\travail-sync\\src\\dataset\\wollongong_step23.csv";
	
	
	
	private static final String CSV_COL_SEPARATOR = ";";

	private static final int numFolds = 3;

	static Logger ll = LoggerFactory.getLogger(Main.class.getSimpleName());
	
	
	
	
	public static void main(String[] args) {
		// theMain(1);

		for (int i = 0; i < numFolds; i++) {
			theMain(i);
			Agent.reset_agent_counter();
		}

	}

	public static void theMain(int foldID) {

		Context.setContextComparisonStrategy(new ContextComparatorImpl());

		LearnerAgent ag1 = null, ag2 = null, ag3 = null;
		try {
			ag1 = AgentFactory.getCrossValidationAgent(CSV_DATASET_FNAME, "6.G01", CSV_COL_SEPARATOR, numFolds,
					foldID, false, ContextInfo.TEMP);
			/*
			 * ag2 = AgentFactory.getCrossValidationAgent(CSV_DATASET_FNAME, "Parma urbana",
			 * CSV_COL_SEPARATOR, numFolds, foldID, true, ContextInfo.TEMP);
			 * 
			 * ag3 = AgentFactory.getCrossValidationAgent(CSV_DATASET_FNAME, "Panocchia",
			 * CSV_COL_SEPARATOR, numFolds, foldID, true, ContextInfo.TEMP);
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}

		ag1.setListener(new AgentListener() {

			@Override
			public void terminated(CrossValidationAgent ag) {

				// double[] pmap =
				// ag.getPerceptions(ContextInfo.TEMP).stream().mapToDouble(x->x).toArray();

				double[] pmap = CrossValidationAgent.getEstimationArray(ag);
				String estimationString = StringUtils.join(ArrayUtils.toObject(pmap), ";");

				String dataString = StringUtils.join(ArrayUtils.toObject(ag.getDataArray(ContextInfo.TEMP)), ";");

				// String r = estimationString;
				String r = estimationString + "\n" + dataString;
				OutputStream os = null;
				try {
					os = new FileOutputStream(new File("C:\\Users\\dguastel\\ESTIMATION_wollongong.csv"));
					os.write(r.getBytes());
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						os.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			@Override
			public void perceived(List<ContextEntry> entries, int currentSampleIdx) {
				
			}

			@Override
			public void addedContext(Context context) {
			}
		});

		// Class s = MultiAgentContextImputation.class;
		Class s = SimpleRegressionImputation.class;

		// try {
		// ag1.setImputationStrategy((ContextImputationStrategy) s.newInstance());
		// ag2.setImputationStrategy((ContextImputationStrategy) s.newInstance());
		// ag3.setImputationStrategy((ContextImputationStrategy) s.newInstance());
		// } catch (InstantiationException | IllegalAccessException e) {
		// e.printStackTrace();
		// }

		/*
		 * ag1.setImputationStrategy(new MultiAgentContextImputation());
		 * ag2.setImputationStrategy(new MultiAgentContextImputation());
		 * ag3.setImputationStrategy(new MultiAgentContextImputation());
		 */

		//
		// ag1.addNeighborAgent(ag2);
		// ag1.addNeighborAgent(ag3);
		//
		// ag2.addNeighborAgent(ag1);
		// ag2.addNeighborAgent(ag3);
		//
		// ag3.addNeighborAgent(ag1);
		// ag3.addNeighborAgent(ag2);
		//
		// ag2.start();
		// ag3.start();
		// ag1.start();

		// ag1.setImputationStrategy(new MultiAgentContextImputation());
		ag1.setImputationStrategy(new ContextVarImputation());
		ag1.start();

	}

}
