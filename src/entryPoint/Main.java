package entryPoint;

import agent.LearnerAgent;
import agent.base.Agent;
import agent.base.AgentFactory;
import agent.base.AgentListener;
import configuration.SystemConfiguration;
import context.Context;
import context.ContextInfo;
import context.comparison.ContextComparatorImpl;
import context.imputation.ContextImputationStrategy;

public class Main {

	public static void main(String[] args) {
		SystemConfiguration.parseParameters(args);

		Context.setContextComparisonStrategy(new ContextComparatorImpl());

		for (int i = 0; i < SystemConfiguration.NUM_FOLDS; i++) {
			// start the agents for the i-th fold
			startAgents(SystemConfiguration.SENSORS_ID_TO_USE, i);

			// reset the agent count in order to keep always the same number of agents
			Agent.reset_agent_counter();
		}
	}

	public static void startAgents(String[] sensorsToUse, int foldID) {

		for (int i = 0; i < sensorsToUse.length; i++) {
			try {
				// Create a new agent
				LearnerAgent ag = AgentFactory.getCrossValidationAgent(SystemConfiguration.CSV_DATASET_FNAME,
						sensorsToUse[i], SystemConfiguration.CSV_COL_SEPARATOR, SystemConfiguration.NUM_FOLDS, foldID,
						false, ContextInfo.TEMP);

				// Set the listener
				ag.setListener(AgentListener.getCrossValidationAgentListener(
						SystemConfiguration.OUT_ESTIMATION_FOLDER + "\\" + sensorsToUse[i] + ".csv"));

				// Set the imputation strategy
				ag.setImputationStrategy(
						(ContextImputationStrategy) SystemConfiguration.IMPUTATION_STRATEGY.newInstance());

				// Start the agent
				ag.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

}
