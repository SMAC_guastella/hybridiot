package agent.crossValidation;

public enum CrossValidationMode {
	TRAINING, TESTING
}
