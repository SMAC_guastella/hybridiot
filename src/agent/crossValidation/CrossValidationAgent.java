package agent.crossValidation;

import java.io.EOFException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import agent.LearnerAgent;
import agent.base.Agent;
import context.Context;
import context.ContextEntry;
import context.ContextInfo;
import context.MissingInformationException;
import sensors.DataSensor;
import sensors.DataSensorFailureHandler;
import sensors.FileDataSensor;

public class CrossValidationAgent extends LearnerAgent {

	/**
	 * The start index of the fold this agent is responsible for testing
	 */
	private int foldStartIndex;

	/**
	 * The end index of the fold this agent is responsible for testing
	 */
	private int foldEndIndex;

	/**
	 * The size of the fold
	 */
	private int foldSize;

	/**
	 * the index of the fold this agent is responsible for estimating
	 */
	private int foldID;

	/**
	 * The operation mode of this agent (training/testing)
	 */
	private CrossValidationMode mode;

	/**
	 * An integer array to be used for cross-validation. A 0 value depicts a
	 * training sample, whereas 1 depicts a test sample
	 */
	private Boolean[] testSetIDX;

	/**
	 * A map containing the data (estimated or not) perceived by sensor. At the end
	 * the double array has the same size of the input data file.
	 */
	private Map<ContextInfo, List<Double>> estimation;

	/**
	 * Estimation array for the entire dataset this agent handles
	 */
	public static Map<String, double[]> ESTIMATION_MAP = new HashMap<>();
	// public static double[] ESTIMATION;

	/**
	 * A boolean variable that indicates whether {@code ESTIMATION} array has been
	 * initialized
	 */
	// private static boolean set = false;
	private static List<String> setList = new ArrayList<>();
	private static boolean set = false;

	/**
	 * Lock object for {@code ESTIMATION} array
	 */
	private static Object lock_estimation = new Object();

	/**
	 * Constructor for the {@code CrossValidationAgent} class.
	 * 
	 * @param dr
	 * @param info
	 */
	private CrossValidationAgent(DataSensor dr, ContextInfo... info) {
		super(dr, info);

		estimation = new HashMap<>();

		for (ContextInfo i : info) {
			estimation.put(i, new ArrayList<>());
		}

		mode = CrossValidationMode.TRAINING;
	}

	/**
	 * Static factory method for {@code CrossValidationAgent} class.
	 * 
	 * @param dr
	 *            a data sensor
	 * @param numFolds
	 *            the number of folds to consider
	 * @param foldID
	 *            the index of the fold to be used for testing
	 * @param info
	 *            the information supported by this agent
	 * @return
	 */
	public static CrossValidationAgent getNew(DataSensor dr, int numFolds, int foldID, ContextInfo... info) {
		return getNew(dr, numFolds, foldID, true, info);
	}

	public static CrossValidationAgent getNew(DataSensor dr, int numFolds, int foldID, boolean trainOnly,
			ContextInfo... info) {
		// Create the agent
		CrossValidationAgent ag = new CrossValidationAgent(dr, info);

		if (!trainOnly) {
			synchronized (lock_estimation) {
				/*
				 * if (!set) { ESTIMATION = new double[dr.getSamplesCount()]; set = true; }
				 */

				String id = ((FileDataSensor) dr).getID();
				if (!setList.contains(id)) {
					// ESTIMATION = new double[dr.getSamplesCount()];
					ESTIMATION_MAP.put(id, new double[dr.getSamplesCount()]);
					setList.add(id);
				}
			}
		}
		// Set the test set indexes
		ag.testSetIDX = getTestSetIDX(ag, numFolds, foldID, dr.getSamplesCount(), trainOnly);

		// Set a new handler for sensor failure
		ag.setSensorFailureHandler(new DataSensorFailureHandler() {

			@Override
			public void handleSensorFailure(Agent a, Exception e, DataSensor s) {

				if (e instanceof EOFException && ag.getCrossValidationMode() == CrossValidationMode.TRAINING) {
					((FileDataSensor) s).resetDataIdx();
					((FileDataSensor) s).setTestSetIDX(ag.testSetIDX);

					ag.observedInfoBuffer.clear();

					// Switch to TESTING mode
					ag.setCrossValidationMode(CrossValidationMode.TESTING);

					System.err.println("SWITCH TO TESTING");
					ag.getLogger().trace("Finished training. Switching to test mode");
				} else {
					if (trainOnly) {
						return; // DO NOT write to file the estimation
					}

					// Take all the values perceived by the agent (both estimated and reals)
					double[] vals = ag.perceptionMap.get(ContextInfo.TEMP).stream().mapToDouble(x -> x).toArray();

					// Take the source and destination indexes where the values have to be saved in
					// the final array
					int srcIDX = ag.foldStartIndex;
					int destPos = ag.foldEndIndex;

					// If the first fold has to be saved
					if (ag.foldID == 0) {
						// save also the data at the first positions
						srcIDX = 0;
					}

					System.err.println("Context size: " + Integer.toString(Context.MAX_CONTEXT_SIZE));

					System.err.println("Estimation array size: "
							+ Integer.toString(ESTIMATION_MAP.get(((FileDataSensor) dr).getID()).length));
					String sensorID = ((FileDataSensor) ag.dataReceiver.get()).getID();
					System.err.println("---------------------> copying >" + Integer.toString(ag.getFoldSize())
							+ "< from " + Integer.toString(srcIDX) + " to " + Integer.toString(destPos) + " for id "
							+ sensorID + "(data samples: " + Integer.toString(ag.dataReceiver.get().getSamplesCount())
							+ ")");

					synchronized (lock_estimation) {
						// copy from srcIDX to srcIDX+len-1 from array vals to the same start position
						// in ESTIMATION
						for (int i = srcIDX; i < destPos; i++) {
							ESTIMATION_MAP.get(((FileDataSensor) dr).getID())[i] = vals[i];
						}
					}

					ag.getLogger().trace("finished cross validation");
					System.out.println("DONE CROSS VALIDATION");

					ag.listener.ifPresent(x -> x.terminated(ag));
					ag.stopAgent();
				}

			}
		});
		return ag;
	}

	/**
	 * Evaluate the array of indexes to be used for evaluating the training/test set
	 * 
	 * @param numFolds
	 * @param foldID
	 * @param dataSamples
	 *            the size of the dataset
	 * @return
	 */
	private static Boolean[] getTestSetIDX(CrossValidationAgent ag, int numFolds, int foldID, int dataSamples,
			boolean trainOnly) {
		// The ID for the fold must be lower of the number of folds
		if (foldID >= numFolds) {
			throw new IllegalArgumentException("fold ID over the number of folds");
		}

		// Size of each fold
		// int foldSize = dataSamples / numFolds;
		int foldSize = new Double(Math.ceil((double) dataSamples / (double) numFolds)).intValue();

		// Index of the first element of the fold
		int foldIDXstart = (foldID * foldSize);

		// Index of the last element of the fold
		int foldIDXend = Math.min(foldIDXstart + foldSize, dataSamples);

		// If the fold is the first, keep the data from 0 to the context size in order
		// to the agent to have a reliable value when starting the estimation step
		if (foldIDXstart < Context.MAX_CONTEXT_SIZE) {
			foldIDXstart += Context.MAX_CONTEXT_SIZE;
		}

		System.err.println("-> fold id: " + Integer.toString(foldID));
		System.err.println("-> fold size: " + Integer.toString(foldSize));
		System.err.println("-> samples count: " + Integer.toString(dataSamples));

		System.err.println(
				"[" + Integer.toString(ag.getAgentId()) + "]--------- FOLD START: " + Integer.toString(foldIDXstart));
		System.err.println(
				"[" + Integer.toString(ag.getAgentId()) + "]--------- FOLD END: " + Integer.toString(foldIDXend));

		ag.foldStartIndex = foldIDXstart;
		ag.foldEndIndex = foldIDXend;
		ag.foldSize = foldSize;
		ag.foldID = foldID;

		// data in interval [foldIDXstart, foldIDend] are used as TEST SET(=1), the
		// other as TRAINING set(=0)
		Boolean[] testSetIdx = new Boolean[dataSamples];

		// start by filling all the test set array to false
		Arrays.fill(testSetIdx, false);
		if (!trainOnly) {
			// Fill to foldIDXend + 1 since toIndex is exclusive
			Arrays.fill(testSetIdx, foldIDXstart, Math.min(foldIDXend + 1, dataSamples), true);
		}

		// Assign the test set vector to the sensor
		return testSetIdx;
	}

	@Override
	protected void addBufferToContext() {
		// if the buffer is empty, do nothing (nothing to add)
		if (observedInfoBuffer.isEmpty()) {
			return;
		}

		if (mode == CrossValidationMode.TRAINING) {
			// ###########################################
			// TRAINING
			// ###########################################
			// try to add the perceived data to the buffer
			try {
				// Remove the first sample in order to insert the next sample and ensure the
				// context to have always the same fixed size
				if (buildingContext.isFull()) {
					buildingContext.removeFirstSample();
				}
				getLogger().trace("[real] Added sequence to context: "
						+ observedInfoBuffer.stream().map(x -> x.toString()).collect(Collectors.joining("; ")));
				buildingContext.addDataSequence(observedInfoBuffer);

			} catch (MissingInformationException e) {
				// If the device has been turned off because of a test sample, clear the current
				// context
				buildingContext.clear();
			}
		} else {
			// ###########################################
			// TESTING
			// ###########################################
			// try to add the perceived data to the buffer
			try {
				// try to add the buffer containing the data perceived from the sensor
				buildingContext.addDataSequence(observedInfoBuffer);
			} catch (MissingInformationException e) {

				// remove the last entries (containing the info to estimate). The imputed
				// information will be put back in the buffer
				buildingContext.removeLastSample();

				// Now fill the context until its size reach the standard context size
				if (!buildingContext.isFull()) {
					fillBuildingContexts(buildingContext);
				}

				// impute missing information
				for (ContextInfo missingInfo : e.getInfo()) {
					// impute missing data
					ContextEntry imputedEntry = imputeMissingData(missingInfo);

					// remove the sample from buffer containing the missing information
					observedInfoBuffer.removeIf(x -> x.isEmpty() && x.getInfo() == missingInfo);

					// get the concerned situation
					observedInfoBuffer.add(imputedEntry);
				}

				getLogger().trace("[estimation] Added sequence to context: "
						+ observedInfoBuffer.stream().map(x -> x.toString()).collect(Collectors.joining("; ")));

				// Add all the estimated values into the context
				buildingContext.forceAddDataSequence(observedInfoBuffer);
			}
		}

		// Take the index of the current data index
		int dataIdx = dataReceiver.get().getCurrentSampleIdx();

		// Add the buffer of perceived data into the perceptions map
		observedInfoBuffer.forEach(x -> perceptionMap.get(x.getInfo()).set(dataIdx, x.getValue()));
	}

	public CrossValidationMode getCrossValidationMode() {
		return mode;

	}

	private void setCrossValidationMode(CrossValidationMode mode) {
		this.mode = mode;
	}

	public int getFoldStartIndex() {
		return foldStartIndex;
	}

	public int getFoldEndIndex() {
		return foldEndIndex;
	}

	public int getFoldSize() {
		return foldSize;
	}

	public double[] getDataArray(ContextInfo info) {
		double[] data = ((FileDataSensor) getRealSensor()).getDataAsDouble(info);
		return data;
	}

	public static double[] getEstimationArray(CrossValidationAgent ag) {
		synchronized (lock_estimation) {
			// return ESTIMATION;
			return ESTIMATION_MAP.get(((FileDataSensor) ag.getRealSensor()).getID());
		}
	}
}
