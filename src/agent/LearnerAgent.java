package agent;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.NotImplementedException;

import agent.base.Agent;
import agent.base.AgentListener;
import context.Context;
import context.ContextEntry;
import context.ContextInfo;
import context.MissingInformationException;
import context.imputation.ContextImputationStrategy;
import sensors.DataSensor;
import sensors.DataSensorFailureHandler;
import sensors.FileDataSensor;

public class LearnerAgent extends Agent {

	public static final int CONTEXT_TO_USE_FOR_ESTIMATION = 5;

	/**
	 * An optional listener for this agent
	 */
	protected Optional<AgentListener> listener;

	/**
	 * Sorted list of contexts previously observed by this agent
	 */
	protected List<Context> contexts;

	/**
	 * Strategy used by this agent to provide an estimation when no real sensor
	 * agents are present in the confidence zone.
	 */
	private Optional<ContextImputationStrategy> imputationStrategy;

	/**
	 * Sensor used to perceive data from the physical environment
	 */
	protected Optional<DataSensor> dataReceiver;

	/**
	 * this buffer contains the information currently sent by sensors to this agent.
	 * These information will be added to the current context
	 */
	protected List<ContextEntry> observedInfoBuffer;

	/**
	 * Last time instant in which data has been added to the current context.
	 */
	private Instant lastAdditionInstant;

	/**
	 * Currently observed context
	 */
	protected Context buildingContext;

	private Optional<DataSensorFailureHandler> sensorFailureHandler;

	/**
	 * Create a new instance of {@code LearnerAgent}. In this case a sensor is
	 * provided as parameter, so the agent is capable of perceiving the physical
	 * environment
	 * 
	 * @param dr
	 */
	public LearnerAgent(DataSensor dr, ContextInfo... info) {
		super();
		contexts = new ArrayList<>();
		observedInfoBuffer = new ArrayList<>();
		for (ContextInfo i : info) {
			perceptionMap.put(i, new ArrayList<>());
		}
		buildingContext = new Context(info);
		lastAdditionInstant = Instant.now();
		imputationStrategy = Optional.empty();
		dataReceiver = Optional.of(dr);
		sensorFailureHandler = Optional.empty();
		listener = Optional.empty();
	}

	// ############################################################################################
	// BEGIN OF REASONING CYCLE METHODS
	// ############################################################################################

	/**
	 * Perceive the data from the physical environment. If a real sensor is
	 * provided, the agent can perceive directly the environment using the sensor.
	 * Otherwise, the data is estimated.
	 */
	@Override
	public void perceive() {
		// [CASE 1] A sensor is provided, this agent do not need to estimate nothing. In
		// this case the data perceived are directly added to the buffer
		if (dataReceiver.isPresent()) {
			perceivePhysicalEnvironment();
		}
	}

	@Override
	public void decideAndAct() {

		// Add the buffer containing the last perceived data into the current context
		addBufferToContext();

		// add the context to the dictionary
		addContextToDictionary();

		if (!buildingContext.isEmpty()) {

			listener.ifPresent(
					x -> x.perceived(buildingContext.getLastSample(), dataReceiver.get().getCurrentSampleIdx()));
		}

		((FileDataSensor) dataReceiver.get()).nextSample();
	}
	// ############################################################################################
	// END OF REASONING CYCLE METHODS
	// ############################################################################################

	/**
	 * Perceive the physical environment by using the provided sensor/data receiver
	 * (if any)
	 */
	private void perceivePhysicalEnvironment() {

		observedInfoBuffer.clear();

		for (ContextInfo info : buildingContext.getSupportedInfo()) {

			// Initialize a new context entry (empty)
			ContextEntry data = ContextEntry.empty(ContextInfo.NULL);

			try {
				// receive data from sensor
				data = dataReceiver.get().receiveData(info);
				// observedInfoBuffer.clear();
				observedInfoBuffer.add(data);

				// listener.ifPresent(x ->
				// x.perceivedDataAtIDX(dataReceiver.get().getSamplesCount(),
				// dataReceiver.get().getCurrentSampleIdx(info)));
			} catch (Exception e) {

				// handle sensor failure
				sensorFailureHandler.ifPresent(x -> x.handleSensorFailure(this, e, dataReceiver.get()));

				// abort the current reasoning cycle because the agent cannot decide and act
				abortReasoning();
			}
		}
	}

	/**
	 * Check if there is at least one pair of agents associated to real sensors in
	 * the confidence zone
	 * 
	 * @return
	 */
	private List<LearnerAgent> getSensorsInConfidenceZone() {
		List<LearnerAgent> ag = new ArrayList<>();
		// TODO here!!
		return ag;
	}

	/**
	 * If a sufficient time laps has passed, the agent can add the informations into
	 * the buffer.
	 * 
	 * @return
	 */
	private boolean isTimeToPerceive() {
		Instant now = Instant.now();
		if (Duration.between(lastAdditionInstant, now).toMillis() < MS_BEFORE_PERCEIVE) {
			return false;
		}
		lastAdditionInstant = Instant.now();
		return true;
	}

	/**
	 * Check if the current observed context has a sufficient number of elements in
	 * order to be added to the dictionary.
	 * 
	 * @return {@code true} if the size of the current context is equals to
	 *         {@code Context.MAX_CONTEXT_SIZE}, {@code false} otherwise.
	 */
	private boolean isTimeToAddContext() {
		return buildingContext.isFull();
	}

	/**
	 * Add the data perceived/estimated to the current observed context
	 */
	protected void addBufferToContext() {
		// if the buffer is empty, do nothing (nothing to add)
		if (observedInfoBuffer.isEmpty()) {
			return;
		}

		try {
			// try to add the buffer containing the data perceived from the sensor
			buildingContext.addDataSequence(observedInfoBuffer);
		} catch (MissingInformationException e) {

			// remove the last entries (containing the info to estimate). The imputed
			// information will be put back in the buffer
			buildingContext.removeLastSample();

			// Now fill the context until its size reach the standard context size
			if (!buildingContext.isFull()) {
				fillBuildingContexts(buildingContext);
			}

			// impute missing information
			for (ContextInfo missingInfo : e.getInfo()) {
				// impute missing data
				ContextEntry imputedEntry = imputeMissingData(missingInfo);

				// remove the sample from buffer containing the missing information
				observedInfoBuffer.removeIf(x -> x.isEmpty() && x.getInfo() == missingInfo);

				// get the concerned situation
				observedInfoBuffer.add(imputedEntry);
			}

			getLogger().trace("[estimation] Added sequence to context: "
					+ observedInfoBuffer.stream().map(x -> x.toString()).collect(Collectors.joining("; ")));

			// Add all the estimated values into the context
			buildingContext.forceAddDataSequence(observedInfoBuffer);
		}

		// Take the index of the current data index
		int dataIdx = dataReceiver.get().getCurrentSampleIdx();

		// Add the buffer of perceived data into the perceptions map
		observedInfoBuffer.forEach(x -> perceptionMap.get(x.getInfo()).set(dataIdx, x.getValue()));
	}

	/**
	 * Return the list of contexts of the neighbor agents.
	 * 
	 * @param numContextToGet
	 *            the number of contexts to get
	 * 
	 * @return a map where the key is an agent and the value is a list of contexts
	 *         observed by the agent
	 */
	protected final Map<Agent, List<Context>> getSimilarContextsFromNeighborsAgent(int numContextToGet,
			Context buildingContext) {
		// Create a new map where the similar contexts are kept for each neighbor agent
		Map<Agent, List<Context>> contextMap = new HashMap<>();

		// iterate for each neighbor agent
		for (Agent a : getNeighbors()) {
			if (!(a instanceof LearnerAgent)) {
				continue;
			}
			// get the most similar contexts and add them to the map
			contextMap.put(a, ((LearnerAgent) a).getMostSimilarContexts(numContextToGet, buildingContext));
		}
		return contextMap;
	}

	/**
	 * Return the last {@code numContextToGet} contexts observed by the neighbor
	 * agents.
	 * 
	 * @param numContextToGet
	 *            the number of contexts to get
	 * @return a map where the key is an agent and the value is a list of contexts
	 *         observed by the agent
	 */
	protected final Map<Agent, List<Context>> getLastContextsFromNeighborsAgent(int numContextToGet) {
		Map<Agent, List<Context>> contextMap = new HashMap<>();
		// prendo i loro ultimi contesti
		for (Agent a : getNeighbors()) {
			if (!(a instanceof LearnerAgent)) {
				continue;
			}
			contextMap.put(a, ((LearnerAgent) a).getLastContext(numContextToGet));

		}
		return contextMap;
	}

	/**
	 * Return the last {@code numContextToGet} contexts observed by this agent.
	 * 
	 * @param numContextToGet
	 *            the number of contexts to get
	 * @return the list of the last {@code numContextToGet} contexts observed by
	 *         this agent.
	 */
	public synchronized List<Context> getLastContext(int numContextToGet) {
		return new ArrayList<>(
				contexts.subList(Math.max(0, contexts.size() - 1 - numContextToGet), contexts.size() - 1));
	}

	public synchronized List<Context> getMostSimilarContexts(int num, Context buildingContext) {
		List<Context> theContexts = new ArrayList<>(contexts);

		// Remove contexts that are equals to the last observed context
		theContexts.removeIf(x -> x.equals(buildingContext));

		List<Context> sortedContexts = new ArrayList<>(theContexts.stream()
				.sorted((c1, c2) -> Double.compare(buildingContext.compare(c1), buildingContext.compare(c2)))
				.collect(Collectors.toList()));

		return sortedContexts.subList(0, Math.min(num, sortedContexts.size()));
	}

	/**
	 * When the building context has a size inferior to the standard context size,
	 * this method fill it with the samples observed from the previous context
	 * 
	 * @param buildingContext
	 *            the context currently observed by this agent
	 */
	protected void fillBuildingContexts(Context buildingContext) {
		if (contexts.isEmpty()) {
			return;
		}
		List<Context> toUse = new ArrayList<>();
		int dataIdx = dataReceiver.get().getCurrentSampleIdx();
		Context contextToUse = contexts.stream().sorted((c1, c2) -> Integer
				.compare(Math.abs(dataIdx - c1.getFinalDataIdx()), Math.abs(dataIdx - c2.getFinalDataIdx())))
				.findFirst().get();

		toUse.add(contextToUse);

		// Ensure the context have a sufficient number of information
		if (!buildingContext.isFull() && !toUse.isEmpty()) {
			int i = 1;
			int j = Context.MAX_CONTEXT_SIZE - 1;

			while (!buildingContext.isFull()) {
				if (j < 0) {
					i--;
					j = Context.MAX_CONTEXT_SIZE - 1;
				}
				buildingContext.forceAddFirst(toUse.get(toUse.size() - i).getSampleAt(j--));
			}
		}
	}

	/**
	 * In case no real sensor is associated to this agent, an estimation is provided
	 * by this function. If other agents are within the confidence zone, these are
	 * used to provide an estimation value. Otherwise, the agent use its previously
	 * observed contexts to provide a reliable estimation value.
	 * 
	 * @param missingInfo
	 *            the type of information to be estimated
	 * 
	 * @return a context entry containing an estimated information
	 */
	protected ContextEntry imputeMissingData(ContextInfo missingInfo) {

		// Get the agents in the confidence zone
		List<LearnerAgent> inCz = getSensorsInConfidenceZone();

		// Create an empty context entry
		ContextEntry imputedEntry = null;

		if (inCz.size() >= 2) {
			// -----------------------------
			// USE AGENTS IN CONFIDENCE ZONE
			// -----------------------------
			throw new NotImplementedException("not implemented yet");
		} else {
			// -----------------------------
			// USE CONTEXTS
			// -----------------------------
			if (!imputationStrategy.isPresent()) {
				throw new IllegalStateException("no imputation strategy provided.");
			}

			// evaluate the context with respect to those previously observed
			try {
				// Put together the contexts of other agents
				Map<Agent, List<Context>> cx = new HashMap<>();

				// NOTA: devo prendere i contesti simili escludendo buildingContext

				// neighbors contexts
				cx.putAll(getSimilarContextsFromNeighborsAgent(CONTEXT_TO_USE_FOR_ESTIMATION, buildingContext));

				// my contexts
				cx.put(this, getMostSimilarContexts(CONTEXT_TO_USE_FOR_ESTIMATION, buildingContext));

				// impute the missing data
				imputedEntry = imputationStrategy.get().impute(this, cx, buildingContext, missingInfo);
			} catch (Exception e) {
				e.printStackTrace();
			}

			return imputedEntry;
		}
	}

	/**
	 * Once enough time has elapsed, the current observed context is added into the
	 * dictionary in case no similar contexts already exists.
	 */
	private void addContextToDictionary() {
		// Check if the context is ready to be added to the dictionary. That is, the
		// number of samples for each situation must be at least equal to
		// Context.MAX_CONTEXT_SIZE
		if (!isTimeToAddContext()) {
			return;
		}

		try {
			Context newContext = (Context) buildingContext.clone();

			// Add to the context the amount of data perceived before processing the
			// context. This information will be used to determine the context to be used to
			// fill the building context in case its number of information is not sufficient
			int finalDataIdx = dataReceiver.get()
					.getCurrentSampleIdx(newContext.getSupportedInfo().stream().findFirst().get());

			newContext.setFinalDataIdx(finalDataIdx);

			// add the new context
			Optional<Context> previous = contexts.stream().filter(x -> x.equals(newContext)).findAny();
			if (!previous.isPresent()) {
				contexts.add(newContext);
				getLogger().trace("Added context: " + newContext.toString());
				listener.ifPresent(x -> x.addedContext(newContext));
			}

			// clear the building context
			buildingContext.clear();

		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Set an imputation strategy for this agent
	 * 
	 * @param strategy
	 */
	public void setImputationStrategy(ContextImputationStrategy strategy) {
		this.imputationStrategy = Optional.of(strategy);
	}

	/**
	 * Return {@code true} if this agent is associated to a real device that can
	 * perceive the physical environment. Otherwise, return {@code false}.
	 * 
	 * @return
	 */
	public boolean isRealSensor() {
		return dataReceiver.isPresent();
	}

	public DataSensor getRealSensor() {
		return dataReceiver.orElseThrow(() -> new IllegalAccessError("no sensor present"));
	}

	public void setListener(AgentListener listener) {
		this.listener = Optional.of(listener);
	}

	public void setSensorFailureHandler(DataSensorFailureHandler h) {
		sensorFailureHandler = Optional.of(h);
	}

	/**
	 * Get all the data collected among the contexts as a double array. Data are
	 * sorted according to the perception instant
	 * 
	 * @param info
	 * @return
	 */
	public double[] getDataAsDouble(ContextInfo info) {
		return perceptionMap.get(info).stream().mapToDouble(x -> x).toArray();
	}

}
