package agent.base;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import agent.crossValidation.CrossValidationAgent;
import context.Context;
import context.ContextEntry;
import context.ContextInfo;

public interface AgentListener {

	void addedContext(Context context);

	void perceived(List<ContextEntry> entries, int currentSampleIdx);

	void terminated(CrossValidationAgent ag);

	/**
	 * Default agent listener used for cross validation agents. The
	 * {@code terminated()} method is overridden to write the estimation array to
	 * the specified input file name
	 * 
	 * 
	 * @param outEstimationFname
	 * @return
	 */
	public static AgentListener getCrossValidationAgentListener(String outEstimationFname) {
		return new AgentListener() {

			@Override
			public void terminated(CrossValidationAgent ag) {
				// Get the estimation array
				double[] pmap = CrossValidationAgent.getEstimationArray(ag);
				// Convert the estimation array to a string
				String estimationString = StringUtils.join(ArrayUtils.toObject(pmap), ";");
				// Do the same with the real data
				String dataString = StringUtils.join(ArrayUtils.toObject(ag.getDataArray(ContextInfo.TEMP)), ";");

				// Assemble data on two lines
				String r = estimationString + "\n" + dataString;
				OutputStream os = null;
				try {
					// Write the data to file
					os = new FileOutputStream(new File(outEstimationFname));
					os.write(r.getBytes());
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						os.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			@Override
			public void perceived(List<ContextEntry> entries, int currentSampleIdx) {
			}

			@Override
			public void addedContext(Context context) {
			}
		};
	}

}
