package agent.base;

import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import context.ContextInfo;

public abstract class Agent extends Thread {
	public static int MS_REASONING_CYCLE_INTERVAL = 1;
	public static int MS_BEFORE_PERCEIVE = 1;

	private static volatile int agentIDcounter = 0;
	private static final Object agentIDcounterLock = new Object();

	private final Optional<Logger> logger;

	/**
	 * Last time instant in which this agent has made a reasoning cycle
	 */
	private Instant lastReasoningCycle;

	/**
	 * 
	 */
	private AgentReasoningProgress progress;

	/**
	 * The list of neighbors agents. To each agent is associated a double that
	 * represents the confidence that this agent has to the agent in this list.
	 */
	private Map<Agent, Double> neighbors;

	/**
	 * the ID of this agent
	 */
	private int id;

	/**
	 * A boolean value that indicates whether this agent is paused
	 */
	private boolean pauseRequested;

	/**
	 * A boolean variable that indicates wheter this agent is active or not.
	 */
	private boolean isActive;

	/**
	 * The perception map of this agent
	 */
	protected Map<ContextInfo, List<Double>> perceptionMap;

	public Agent() {
		neighbors = new HashMap<>();
		lastReasoningCycle = Instant.now();
		progress = AgentReasoningProgress.IDLE;
		pauseRequested = false;
		isActive = false;
		perceptionMap = new HashMap<>();
		logger = Optional.ofNullable(
				LoggerFactory.getLogger(Agent.class.getSimpleName() + " #" + Integer.toString(agentIDcounter)));

		synchronized (agentIDcounterLock) {
			id = agentIDcounter++;
		}
	}

	/**
	 * Return the identifier of this agent
	 * 
	 * @return
	 */
	public int getAgentId() {
		return id;
	}

	/**
	 * Get the neighbors agents
	 * 
	 * @return
	 */
	public List<Agent> getNeighbors() {
		return neighbors.keySet().stream().collect(Collectors.toList());
	}

	/**
	 * Add a neighbor and associate a confidence value of {@code 1}.
	 * 
	 * @param ag
	 * @return true if the agent is associated. False if the agent is already in the
	 *         neighbor list.
	 */
	public boolean addNeighborAgent(Agent ag) {
		if (ag.equals(this) || neighbors.containsKey(ag)) {
			return false;
		}

		neighbors.put(ag, 1.0);
		return true;
	}

	/**
	 * Dissociate from the specified agent.
	 * 
	 * @param ag
	 * @return the last confidence value between this agent the input agent.
	 *         Otherwise, return {@code NaN}.
	 */
	public double dissociateFrom(Agent ag) {
		if (!neighbors.containsKey(ag)) {
			return Double.NaN;
		}

		return neighbors.remove(ag);
	}

	/**
	 * Modify the confidence value of this agent to the specified agent. The
	 * confidence is modified according to the delta value provided in input
	 * (positive or negative)
	 * 
	 * @param ag
	 * @param delta
	 * @return the previous confidence value associated with {@code ag}, or NaN if
	 *         this agent is not associated to {@code ag}.
	 */
	public double modifyConfidence(Agent ag, double delta) {
		if (!neighbors.containsKey(ag)) {
			return Double.NaN;
		}
		return neighbors.put(ag, neighbors.get(ag) + delta);
	}

	@Override
	public final void run() {
		isActive = true;
		resumeAgent();
		reasoningCycleMain();
	}

	private final void reasoningCycleMain() {
		while (isActive) {
			if (Duration.between(lastReasoningCycle, Instant.now()).toMillis() < MS_REASONING_CYCLE_INTERVAL) {
				continue;
			}

			if (progress == AgentReasoningProgress.IDLE) {
				lastReasoningCycle = Instant.now();
				getLogger().trace("Reasoning cycle. TS: " + lastReasoningCycle.toString());
				continue;
			}

			// If reasoning cycle was previously aborted, restore to READY state
			if (progress == AgentReasoningProgress.ABORT) {
				progress = AgentReasoningProgress.READY;
			}

			// if ready, perceive
			if (progress == AgentReasoningProgress.READY) {
				perceive();
				getLogger().trace("Perceive OK");
				// If not aborted, proceed
				if (progress == AgentReasoningProgress.ABORT) {
					lastReasoningCycle = Instant.now();
					continue;
				}
				progress = AgentReasoningProgress.PERCEIVE_OK;
			}

			// If perception OK
			if (progress == AgentReasoningProgress.PERCEIVE_OK) {

				// decide and act
				decideAndAct();

				getLogger().trace("Decide&Act OK");

				// check if aborted
				if (progress == AgentReasoningProgress.ABORT) {
					lastReasoningCycle = Instant.now();
					continue;
				}

				if (pauseRequested) {
					getLogger().trace("Paused");
					progress = AgentReasoningProgress.IDLE;
				} else {
					progress = AgentReasoningProgress.READY;
				}

			}
			lastReasoningCycle = Instant.now();
		}
	}

	/**
	 * 
	 */
	public abstract void perceive();

	/**
	 * 
	 */
	public abstract void decideAndAct();

	protected final void abortReasoning() {
		progress = AgentReasoningProgress.ABORT;
		// TODO log abort
	}

	public void stopAgent() {
		isActive = false;
	}

	public boolean isActive() {
		return isActive;
	}

	public final synchronized void resumeAgent() {
		progress = AgentReasoningProgress.READY;
		pauseRequested = false;

	}

	public final synchronized void pauseAgent() {
		pauseRequested = true;
	}

	protected Logger getLogger() {
		return logger.orElseThrow(() -> new IllegalStateException("No logger found"));
	}

	@Override
	public String toString() {
		return "Agent [id=" + id + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Agent other = (Agent) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public static void reset_agent_counter() {
		synchronized (agentIDcounterLock) {
			agentIDcounter = 0;
		}
	}

}
