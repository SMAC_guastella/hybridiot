package agent.base;

public enum AgentReasoningProgress {
	IDLE, PERCEIVE_OK, DECIDE_ACT_OK, ABORT, READY
}
