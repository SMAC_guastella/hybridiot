package agent.base;

import java.util.Arrays;
import java.util.Collections;

import agent.LearnerAgent;
import agent.crossValidation.CrossValidationAgent;
import context.ContextInfo;
import sensors.DataSensor;
import sensors.FileDataSensor;

public class AgentFactory {

	public static LearnerAgent getRealAgent(String fname, String sensorID, String columnSeparator, ContextInfo... info)
			throws Exception {

		if (info.length == 0) {
			throw new IllegalArgumentException("No context information provided");
		}

		// Create the sensor
		DataSensor dr = FileDataSensor.getNew(fname, sensorID, columnSeparator);

		// delete duplicate info
		ContextInfo[] ui = Arrays.stream(info).distinct().toArray(ContextInfo[]::new);

		// Create and return a new agent
		return new LearnerAgent(dr, ui);
	}

	/**
	 * Create a new agent for cross-validating a given dataset (as CSV file)
	 * 
	 * @param fname
	 *            the dataset to be used
	 * @param sensorID
	 *            the ID of the sensor to use (it corresponds to the row of the
	 *            dataset to be read)
	 * @param columnSeparator
	 *            the separator character used by the CSV file
	 * @param numFolds
	 *            the number of folds
	 * @param foldID
	 *            the ID of the fold to treat ([0..{@code numFolds}))
	 * @param info
	 * @return
	 * @throws Exception
	 */
	public static LearnerAgent getCrossValidationAgent(String fname, String sensorID, String columnSeparator,
			int numFolds, int foldID, boolean trainOnly, ContextInfo... info) throws Exception {

		if (info.length == 0) {
			throw new IllegalArgumentException("No context information provided");
		}

		// Create the sensor
		DataSensor dr = FileDataSensor.getNew(fname, sensorID, columnSeparator);

		System.out.println("----------------------------- SAMPLES [" + ((FileDataSensor) dr).getID() + "]: "
				+ Integer.toString(dr.getSamplesCount()));

		// delete (eventual) duplicate info
		ContextInfo[] ui = Arrays.stream(info).distinct().toArray(ContextInfo[]::new);

		// Create the agent
		LearnerAgent ag = CrossValidationAgent.getNew(dr, numFolds, foldID, trainOnly, ui);

		// Initialize the perception map of the agent with arrays of size equals to the
		// size of the dataset
		ag.perceptionMap.forEach((k, v) -> v.addAll(Collections.nCopies(dr.getSamplesCount() + 1, 0.0)));

		// return the new agent
		return ag;
	}

}
