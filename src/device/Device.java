package device;

import context.ContextInfo;

public interface Device {
	boolean isActive();

	void turnOn();

	void turnOff();
	
	void turnOn(ContextInfo info);

	void turnOff(ContextInfo info);
}
